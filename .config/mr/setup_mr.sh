#!/bin/bash

init() {
  lc_all_bak=${LC_ALL}
  language_bak=${LANGUAGE}
  export LANGUAGE="en_US.UTF-8"
  export LC_ALL="en_US.UTF-8"
}

quit() {
  local ret_code=$1
  export LC_ALL=${lc_all_bak}
  export LANGUAGE=${language_bak}
  if [[ ${ret_code} -eq 1 ]]
  then
    echo "Something went wrong. Installation aborted."
  fi
  exit "$1"
}

install() {
  local pkg=$1
  if uname -v | grep -q "Ubuntu"
  then
    sh -c "sudo apt-get install -y $pkg"
  fi
}

test_required_pkg() {
  local cmd=$1
  local pkg=$2

  if ! type "$cmd" >/dev/null 2>&1
  then
    echo " \
 ERROR ========================================================================
You do not have ${pkg} installed which is required.
Do you want to install it [Y/n] ?
==============================================================================="
    read YN
    YN=${YN:-y}
    if [[ "${YN}" =~ (Y|y) ]]
    then
      install "${pkg}"
    else
      echo "\
  ERROR ========================================================================
Installation aborted.
==============================================================================="
      quit 1
    fi
  fi
  return 0
}

test_pkg() {
  local cmd=$1
  local pkg=$2
  if ! type "$cmd" >/dev/null 2>&1
  then
    echo " \
 WARNING =======================================================================
You do not have ${pkg} installed, this package is especially required.
Do you want to install it [Y/n] ?
==============================================================================="
    read YN
    YN=${YN:-y}
    if [[ "${YN}" =~ (Y|y) ]]
    then
      install pkg
    else
      echo "\
 WARNING ======================================================================
Dotfiles installation will continue without ${pkg}.
==============================================================================="
      return 1
    fi
  fi
  return 0
}

parse_url () {
  local repo_url=$1
  if echo "${repo_url}" | grep -q 'git@'
  then
    url=${repo_url%%:*}
    url=${url##*@}
    usr=${repo_url%%/*}
    usr=${usr##*:}
    repo=${repo_url##*/}
  elif echo "${repo_url}" | grep -q 'gogs@'
  then
    url=${repo_url%%:*}
    url=${url##*@}
    usr=${repo_url%%/*}
    usr=${usr##*:}
    repo=${repo_url##*/}
  elif echo "${repo_url}" | grep -q "https"
  then
    url=${repo_url##*://}
    url=${url%%/*}
    usr=${repo_url%%/${repo}}
    usr=${usr##*/}
    repo=${repo_url##*/}
  else
    echo "Sorry, but url : ${repo_url} is not supported yet"
    read
  fi
}

set_email() {
  local mail1=''
  local mail2=''
  local mail_regex="^[a-z0-9!#\$%&'*+/=?^_\`{|}~-]+(\.[a-z0-9!#$%&'*+/=?^_\`{|}~-]+)*@([a-z0-9]([a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]([a-z0-9-]*[a-z0-9])?\$"

  while true
  do
    echo "Please enter the email adress for the user $( whoami ) : "
    read mail1

    if [[ ! ${mail1} =~ ${mail_regex} ]]
    then
      echo "\
 ERROR ========================================================================
This is not an email adress. Please enter one of the form :
     email@domain.com.
==============================================================================="
    else
      echo "Please enter email adress again"
      read mail2

      if [[ ! ${mail1} == "${mail2}" ]]
      then
        echo "\
 ERROR ========================================================================
Emails do not match. Do you want to retry [Y/n] ?
==============================================================================="
        read YN
        YN=${YN:-y}
        if ! [[ "${YN}" =~ (Y|y) ]]
        then
          quit 1
        fi
      elif [[ ${mail1} == "${mail2}" ]]
      then
        user_mail=${mail2}
        chfn -r "${user_mail}" "${username}"
      fi
    fi
  done
}

gen_ssh_key() {
  local username;
  local user_mail;
  local id_rsa_file

  username="$( whoami )"
  user_mail=$( grep "^$username:" /etc/passwd | cut -d: -f5 | cut -d, -f2 )
  id_rsa_file="${HOME}/.ssh/id_rsa"

  if [[ ${#user_mail} -eq 0 ]]
  then
    set_email
  fi

  ssh-keygen -t rsa -b 4096 -C "${user_mail}" -f "${id_rsa_file}"

  clear
  echo "\
Next screen will be the content of your public key :
    ${id_rsa_file}.pub.
You can copy it and paste it to you favorite version control system/website.

NOTE : If you are not connected through SSH this can be complicated for you.
Unfortunately, there is nothing the script can do for you, but you can do it
manually by searching 'add SSH Key github' in your favorite web searcher.
Press 'Enter' to continue"
  read

  clear
  echo "======================================="
  cat "${id_rsa_file}.pub"
  echo "======================================="
  echo "Press 'Enter' to continue"
  read
  return 0
}

setup_ssh_key() {
  while true
  do
    clear
    echo "\
 ERROR ========================================================================
You want to install your versionned file using ssh, but there is no file *.pub
in ${HOME}/.ssh/.

Do you want to :
    1) Setup a new ssh key at ${HOME}/.ssh/id_rsa
    2) Abort installation
===============================================================================
Default 1 [1/2/3] :"
    read input
    input=${input:-1}
    case ${input} in
    1)
      gen_ssh_key
      return 0
      ;;
   2)
      echo "Aborting installation."
      quit 1
      ;;
    *)
      echo "Wrong input, please enter 1, 2 or 3."
      echo "Press 'Enter' to continue"
      read
      ;;
    esac
  done
}

check_ssh_key() {
  if ! [[ -e "${HOME}/.ssh/" ]]
  then
    setup_ssh_key
  elif ! find "${HOME}/.ssh/" -name "*.pub" >/dev/null
  then
    setup_ssh_key
  fi
}

load_ssh_key() {
  local files

  clear
  echo "\
===============================================================================
You choose to use SSH method to clone your repos, in order to clone them easier
your SSH key will be loaded with keychain.
You will be asked to enter your SSH key password
==============================================================================="
  file=""
  file=$( find $HOME/.ssh/ -type f -name "*.pub" )
  file=${FILE//\.pub/}
  eval "$(keychain --eval --agents ssh "${files}" )"
  return 0
}

add_netrc_config() {
  local machine
  local login
  local password

  while true
  do
    clear
    echo "\
===============================================================================
Please enter the url of the machine :"
    read machine
    echo "\
Please enter the login to use :"
    read login
    echo "\
Please enter the password to use (will be hidden) :"
    read -s password
    clear
    echo "\
===============================================================================
Are these informations are correct [Y/n] ?
machine ${machine}
  login ${login}
  password (the one you set)"
    read YN
    YN=${YN:-y}
    if [[ "${YN}" =~ (Y|y) ]]
    then
      echo "\
machine ${machine}
  login ${login}
  password ${password}" >> "${HOME}/.netrc"
    fi
    clear
    echo "\
===============================================================================
Do you want to add another config to your ~/.netrc [Y/n] ?"
    read YN
    YN=${YN:-y}
    if ! [[ "${YN}" =~ (Y|y) ]]
    then
      return 1
    fi
  done
}

set_netrc() {
  clear
  echo "\
===============================================================================
You choose to use HTTPS method to clone your repos, in order to clone them
easier, do you want to use ~/.netrc file [Y/n] ?
WARNING : Unfortunately, this file will be clear plain text and can present
some security issue. If you can, prefer cloning files with SSH key.
==============================================================================="
  read YN
  YN=${YN:-y}
  if [[ "${YN}" =~ (Y|y) ]]
  then
    add_netrc_config
  fi
}

install_symlink() {
  local path=$1
  local prefix=${2:-"$HOME"}

  for file in ${path}/{.*,*}
  do
    if   ! [[ ${file} == "${path}/." ]] \
      && ! [[ ${file} == "${path}/*" ]] \
      && ! [[ ${file} == "${path}/.*" ]] \
      && ! [[ ${file} == "${path}/.." ]] \
      && ! [[ ${file} == "${path}/.git" ]] \
      && ! [[ ${file} == "${path}/.gitignore.d" ]]
    then
      if [[ ${file} == "${path}/.config" ]]
      then
        install_symlink "${path}/.config" "${prefix}/.config"
      else
        if [[ -e "${prefix}/${file##*/}" ]]
        then
          mv "${prefix}/${file##*/}" "${prefix}/.dotfiles.bak"
        fi
        ln -s "${file}" "${prefix}/${file##*/}"
      fi
    fi
  done
}

parse_yaml() {
  local prefix=$2
  local s
  local w
  local fs
  s='[[:space:]]*'
  w='[a-zA-Z0-9_]*'
  fs="$(echo @|tr @ '\034')"
  sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
       -e "s|^\($s\)\($w\)$s[:-]$s\(.*\)$s\$|\1$fs\2$fs\3|p" "$1" |
    awk -F"$fs" '{
      indent = length($1)/2;
      if (length($2) == 0) { conj[indent]="+";} else {conj[indent]="";}
        vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
        if (length($3) > 0) {
          vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
      printf("%s%s%s%s=(\"%s\")\n", "'"$prefix"'",vn, $2, conj[indent-1],$3);
    }
  }' | sed 's/_=/+=/g'
}

parse_subdir() {
  local a_subdir_ssh_url=""
  local a_subdir_https_url=""
  local a_subdir_path=""
  local a_subdir_name=""
  local subdir_url=""
  local subdir_path=""
  local subdir_name=""
  subdir_update=""
  subdir_pull=""
  subdir_del=""

  IFS=',' read -r -a a_subdir_ssh_url <<< "${repos__subdir_ssh_url[idx]}"
  IFS=',' read -r -a a_subdir_https_url <<< "${repos__subdir_https_url[idx]}"
  IFS=',' read -r -a a_subdir_path <<< "${repos__subdir_path[idx]}"
  IFS=',' read -r -a a_subdir_name <<< "${repos__subdir_name[idx]}"
  for (( iSubdir=0; iSubdir < ${#a_subdir_name[@]}; iSubdir++ ))
  do
    if [[ ${method} == "ssh" ]] \
      && ! [[ ${a_subdir_ssh_url[iSubdir]} == "none" ]]
    then
      subdir_url=${a_subdir_ssh_url[iSubdir]}
    elif [[ ${method} == "ssh" ]] \
      && [[ ${a_subdir_ssh_url[idx]} == "none" ]] \
      && ! [[ ${a_subdir_https_url} == "none" ]]
    then
      subdir_url=${a_subdir_https_url[idx]}
      do_set_netrc=true
    elif [[ ${method} == "https" ]] \
      && ! [[ ${a_subdir_https_url[idx]} == "none" ]]
    then
      subdir_url=${a_subdir_https_url[idx]}
    elif [[ ${method} == "https" ]] \
      && [[ ${a_subdir_https_url[idx]} == "none" ]] \
      && ! [[ ${a_subdir_ssh_url} == "none" ]]
    then
      subdir_url=${a_subdir_ssh_url[idx]}
      do_set_shh_key=true
    else
      echo "\
  WARNING =====================================================================
There is no valid SSH url, nor HTTPS url for repos ${subdir_name}.
Therefore, this repo will not be installed
==============================================================================="
      subdir_valid_url=false
    fi

    if ${subdir_valid_url}
    then
      subdir_url=${subdir_url//\//\\\/}
      subdir_path=${a_subdir_path[iSubdir]}
      subdir_path=${subdir_path//\//\\\/}

      subdir_update+="\
            mkdir -p ${subdir_path};\\n\
            git clone ${subdir_url} ${a_subdir_path}/${a_subdir_name[iSubdir]};\\n"
      subdir_pull+="\
            cd ${subdir_path}\\/${a_subdir_name[iSubdir]};\\n\
            git pull;\\n"
      subdir_del+="\
            rm -rf ${subdir_path}\\/${a_subdir_name[iSubdir]};\\n"
    fi
  done
  subdir_update=${subdir_update:0:$(( ${#subdir_update} - 3 ))}
  subdir_pull=${subdir_pull:0:$(( ${#subdir_pull} - 3 ))}
  subdir_del=${subdir_del:0:$(( ${#subdir_del} - 3 ))}
}

config_mr() {
  local path
  local repo
  local usr
  local url
  local full_url
  local remote
  local do_set_netrc
  local do_set_shh_key
  local subdir_update
  local subdir_pull
  local subdir_del
  local subdir_valid_url

  do_set_netrc=false
  do_set_shh_key=false
  subdir_valid_url=true

  parse_yaml "${HOME}/.config/mr/repo_list.yml" > repo.sh
  source repo.sh
  rm -rf "${HOME}"/.config/mr/repo.d/*.* repo.sh

  for (( idx=0 ; idx < ${#repos__name[@]}; idx++ ))
  do
    if [[ ${method} == "ssh" ]] \
      && ! [[ ${repos__ssh_url[idx]} == "none" ]]
    then
      parse_url "${repos__ssh_url[idx]}"
      full_url=${repos__ssh_url[idx]}
    elif [[ ${method} == "ssh" ]] \
      && [[ ${repos__ssh_url[idx]} == "none" ]] \
      && ! [[ ${repos__https_url} == "none" ]]
    then
      parse_url "${repos__https_url[idx]}"
      full_url=${repos__https_url[idx]}
      do_set_netrc=true
    elif [[ ${method} == "https" ]] \
      && ! [[ ${repos__https_url[idx]} == "none" ]]
    then
      parse_url "${repos__https_url[idx]}"
      full_url=${repos__https_url[idx]}
    elif [[ ${method} == "https" ]] \
      && [[ ${repos__https_url[idx]} == "none" ]] \
      && ! [[ ${repos__ssh_url} == "none" ]]
    then
      parse_url "${repos__https_url[idx]}"
      full_url=${repos__ssh_url[idx]}
      do_set_shh_key=true
    else
      echo "\
  WARNING =====================================================================
There is no valid SSH url, nor HTTPS url for repos ${repos__name[idx]}.
Therefore, this repo will not be installed
==============================================================================="
      subdir_valid_url=false
    fi

    if ${subdir_valid_url}
    then
      if [[ ${repos__type[idx]} == "vcsh" ]] && type vcsh >/dev/null 2>&1
      then
        path="${HOME}/.config/vcsh/repo.d/${repos__name[idx]}.git"
        cp "${HOME}/.config/mr/repo.d/template_vcsh" \
           "${HOME}/.config/mr/repo.d/${repos__name[idx]}.${repos__type[idx]}"
      elif [[ ${repos__type[idx]} == "vcsh" ]] && ! type vcsh >/dev/null 2>&1
      then
        path="${HOME}/.dotfiles/${repos__name[idx]}"
        a_symlinks+=("${HOME}/.dotfiles/${repos__name[idx]}")
        cp "${HOME}/.config/mr/repo.d/template_git" \
           "${HOME}/.config/mr/repo.d/${repos__name[idx]}.${repos__type[idx]}"
      elif ! [[ ${repos__type[idx]} == "vcsh" ]]
      then
        path="${repos__path[idx]}/${repos__dirname[idx]}"
        if [[ -e "${HOME}/.config/mr/repo.d/template_${repos__type[idx]}" ]]
        then
          cp "${HOME}/.config/mr/repo.d/template_${repos__type[idx]}" \
             "${HOME}/.config/mr/repo.d/${repos__name[idx]}.${repos__type[idx]}"
        else
          cp "${HOME}/.config/mr/repo.d/template_git" \
             "${HOME}/.config/mr/repo.d/${repos__name[idx]}.${repos__type[idx]}"
        fi
      fi
      remote="${url%.*}.${usr}"

      sed -i -e "s|<TPL:PATH>|${path}|g" \
             -e "s|<TPL:REMOTE>|${remote}|g" \
             -e "s|<TPL:URL>|${full_url}|g" \
             -e "s|<TPL:NAME>|${repos__name[idx]}|g" \
        "${HOME}/.config/mr/repo.d/${repos__name[idx]}.${repos__type[idx]}"

      if ! [[ "${repos__subdir_name[idx]}" == "none" ]]
      then
        parse_subdir
        sed -i -e "/<TPL:SUBDIR_UPDATE>/i \\${subdir_update}" \
               -e "/<TPL:SUBDIR_PULL>/i \\${subdir_pull}" \
               -e "/<TPL:SUBDIR_DELETE>/i \\${subdir_del}" \
          "${HOME}/.config/mr/repo.d/${repos__name[idx]}.${repos__type[idx]}"
      fi
      sed -i -e '/<TPL:SUBDIR_UPDATE>/d' \
             -e '/<TPL:SUBDIR_PULL>/d' \
             -e '/<TPL:SUBDIR_DELETE>/d' \
        "${HOME}/.config/mr/repo.d/${repos__name[idx]}.${repos__type[idx]}"
    fi
  done

  if ${do_set_netrc}
  then
    echo "\
 WARNING ======================================================================
Some package have only HTTPS url. Thus, you will be ask if you wan to setup
a ~/.netrc file.
==============================================================================="
    set_netrc
  elif ${do_set_shh_key}
  then
    echo "\
 WARNING ======================================================================
Some package have only SSH url. Thus, you will be ask if you wan to setup
a SSH Key
==============================================================================="
    setup_ssh_key
  fi
  return 0
}

install_mr_config() {
  local url
  local usr

  parse_url "${mr_repo}"

  if type vcsh >/dev/null 2>&1
  then
    cd "${HOME}"
    vcsh clone "${mr_repo}" mr
    vcsh mr remote rename origin "${url%.*}.${usr}"
    vcsh mr push -u "${url%.*}.${usr}" master
  else
    mkdir -p "${HOME}/.dotfiles" "${HOME}/.dotfiles.bak"
    cd "${HOME}/.dotfiles/mr"
    git clone "${mr_repo}" "${HOME}/.dotfiles/mr"
    git remote rename origin "${url%.*}.${usr}"
    git push -u "${url%.*}.${usr}" master
    install_symlink "${HOME}/.dotfiles/mr"
    cd "${HOME}"
  fi

  return 0
}

set_method() {
  local input
  while true
  do
    clear
    echo "\
===============================================================================
  Do you want to :
    1) Clone using ssh
          git@bitbucket.org:vcsh/mr.git mr
    2) Clone using https
          https://bitbucket.org/vcsh/mr.git
    3) Abort installation
===============================================================================
Default 1 [1/2/3] :"
    read input
    input=${input:-1}
    case ${input} in
    1)
      method="ssh"
      mr_repo="${SSH_REPO}"
      return 0
      ;;
    2)
      method="https"
      mr_repo="${HTTPS_REPO}"
      return 0
      ;;
    3)
      echo "Aborting installation."
      quit 1
      ;;
    *)
      echo "Wrong input, please enter 1, 2 or 3."
      echo "Press 'Enter' to continue"
      read
      ;;
    esac
  done
}

choose_repo () {
  local menu_app
  local result_menu
  local choice
  local ext=$1

  menu_app="whiptail --title 'Init Host myRepo' \
  --checklist  'Select which ${ext} repo you want you want for this host :' \
  $(( ${#repos__name[@]} + 10 )) 80 ${#repos__name[@]}"
  for (( iRepo=0; iRepo < ${#repos__name[@]}; iRepo++ ))
  do
    if [[ "${repos__type[iRepo]}" == "${ext}" ]]
    then
      if [[ -e "$HOME/.config/mr/config.d/$( hostname ).cfg" ]] \
        && grep -q "${repos__name[iRepo]}" "$HOME/.config/mr/config.d/$( hostname ).cfg"
      then
        menu_app="${menu_app} '${repos__name[iRepo]}' '${repos__desc[iRepo]}' 'ON'"
      else
        menu_app="${menu_app} '${repos__name[iRepo]}' '${repos__desc[iRepo]}' 'OFF'"
      fi
    fi
  done

  bash -c "${menu_app}" 2> results_menu.txt
  if [[ $? -eq 1 ]]
  then
    rm results_menu.txt
    return 1
  fi

  result_menu=( $( cat results_menu.txt) )
  for (( i=0 ; i < ${#result_menu[@]} ; i++))
  do
    choice=${result_menu[i]//\"/}
    echo "include = cat \$HOME/.config/mr/repo.d/${choice}.${ext}" >> "${outfile}"
  done
  rm results_menu.txt
  return 0
}

update_cfg () {
  local outfile
  local a_types
  local repo_type

  outfile="${HOME}/.config/mr/config.d/$( hostname ).cfg.tmp"

  echo "# Configuration file for $( hostname )" > "${outfile}"
  for iType in ${HOME}/.config/mr/repo.d/*.*
  do
    repo_type=${iType##*.}
    if ! [[ "${a_types[@]}" =~ "$repo_type" ]]
    then
      a_types+=("${repo_type}")
    fi
  done

  for iType in "${a_types[@]}"
  do
    choose_repo "${iType}"
    if [[ $? -eq 1 ]]
    then
      rm "${outfile}"
      return 1
    fi
  done

  mv -f "${outfile}" "${outfile%%.tmp}" > /dev/null
}

config_host_mr() {
  if [[ -e "${HOME}/.config/mr/config.d/$( hostname ).cfg" ]]
  then
    if  ( whiptail --title 'Init Host myrepo' \
      --yesno "This computer with hostname $( hostname ) seems to be already configure.
Do you want to update configuration configure it now ? " 10 60 )
    then
      update_cfg
      return $?
    else
      return 0
    fi
  elif ( whiptail --title 'Init Host myrepo' \
    --yesno "This computer with hostname $( hostname ) does not seem to be configure.
Do you want to configure it now ? " 10 60 )
  then
    update_cfg
    return $?
  else
    touch "${HOME}/.config/mr.config.d/$( hostname ).cfg"
    return 1
  fi
}

config_host_default() {
  local config_file=""
  local counter=0
  local choice=""
  local config=()
  local desc=""
  local name=""

  config_file="$HOME/.config/mr/config.d/$( hostname ).cfg"

  menu="\
-------------------------------------------------------------------------------
Which type of default config you want to set for this host : "

  for iConf in ${HOME}/.config/mr/config.d/*.defaults
  do
    name=${iConf%%.defaults}
    name=${name##*_}
    desc=$( grep "#Desc:" "${iConf}" | cut -d: -f2 )
    menu+="
    ${counter}) ${name} - ${desc}"
    choice+="${counter}/"
    config+=("${name}")
    (( counter++ ))
  done
  choice+="${counter}"
  menu+="
    ${counter}) Abort installation
Default 0 [${choice}]:"
  while true
  do
    echo "${menu}"
    read choice
    choice=${choice:-1}
    if [[ ${choice} -eq ${counter} ]]
    then
      return 1
    elif [[ ${choice} -lt ${counter} ]]
    then
      sh -c "cp ${HOME}/.config/mr/config.d/*${config[choice]}.defaults ${config_file}"
      return 0
    else
      echo "Wrong input, please enter a unique number between 0 and ${counter}"
    fi
  done
}

emulate_mr() {
  while read line
  do
    file=${line##include = cat }
    file=${file/\$HOME/$HOME}
    BEGIN=$( grep -in "checkout    =" "$file" | cut -d ':' -f1 )
    END=$(   grep -in "pull        =" "$file" | cut -d ':' -f1 )
    (( BEGIN++ ))
    (( END-- ))
    bash -c "$(sed -n "${BEGIN},${END}p" "$file" )"
    echo
  done <<< "$( grep include "$HOME/.config/mr/config.d/$( hostname ).cfg" )"
}

setup_mr() {
  local repos__name
  local repos__desc
  local repos__ssh_url
  local repos__https_url
  local repos__path
  local repos__dirname
  local repos__type
  local repos__subdir_ssh_url
  local repos__subdir_https_url
  local repos__subdir_path
  local repos__subdir_name
  local lc_all_bak
  local language_bak
  local method
  local mr_repo
  local a_symlinks

  SSH_REPO="gogs@git.romaindeville.fr:vcsh/mr.git"
  HTTPS_REPO="https://git.romaindeville.fr/vcsh/mr.git"

  test_required_pkg git git
  test_pkg mr myrepos
  test_pkg vcsh vcsh

  if [[ $? -eq 1 ]]
  then
    echo "\
 WARNING ======================================================================
vcsh is not installed, thus, your dotfiles will be installed into folder
${HOME}/.dotfiles
and will be setup using symlinks. Your old dotfiles will be back up in
${HOME}/.dotfiles.bak
==============================================================================="
  fi

  set_method

  if [[ ${method} == "ssh" ]]
  then
    check_ssh_key
    if test_pkg keychain keychain
    then
      load_ssh_key
    fi
  elif [[ ${method} == "https" ]]
  then
    set_netrc
  fi

  install_mr_config

  config_mr

  if type whiptail >/dev/null 2>&1
  then
    config_host_mr
  else
    config_host_default
  fi

  if type mr >/dev/null 2>&1
  then
    mr up
  else
    emulate_mr
  fi

  for (( iPath=0; iPath < ${#a_symlinks[@]}; iPath++ ))
  do
    install_symlink "${a_symlinks[iPath]}"
  done

  quit 0
}

setup_mr
