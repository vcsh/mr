#!/usr/bin/env python3

import os
import apt
import sys
import yaml
import socket
import curses
import re
from urllib.parse import urlparse

class MyRepo:
    def __init__(self, conf_file):
        """
        TODO RD : Write doc
        """
        self.env = {}
        self.conf = self._load_config(self,conf_file)
        self.home_dir = os.path.expanduser('~')
        self.vcsh_dir = self.home_dir + "/.config/vcsh/"
        self.mr_dir = self.home_dir + "/.config/mr/"
        if hasattr(os.environ, "LC_ALL"):
            self.env["LC_ALL"] = os.environ["LC_ALL"]
        else:
            self.env["LC_ALL"] = ""
        if hasattr(os.environ, "LANGUAGE"):
            self.env["LANGUAGE"] = os.environ["LANGUAGE"]
        else:
            self.env["LANGUAGE"] = ""
        os.environ["LC_ALL"] = "en_US.UTF-8"
        os.environ["LANGUAGE"] = "en_US.UTF-8"

    def __del__(self):
        """
        TODO RD : Write doc
        """
        os.environ["LC_ALL"] = self.env["LC_ALL"]
        os.environ["LANGUAGE"] = self.env["LANGUAGE"]

    def quit(self, return_code):
        """
        TODO RD : Write doc
        """
        if return_code is not 0:
            print("Something went wrong. Installation aborted")

    def _check_package(self, package):
        """
        TODO RD : Write doc
        """
        cache = apt.cache.Cache()
        cache.update()
        cache.open()
        pkg = cache[package]
        if not pkg.is_installed:
            pkg.mark_install()
        #try:
        cache.commit()
        #except Exception,arg:
        #    print("Sorry, package installation failed [{err}]".format(err=str(arg))

    @staticmethod
    def _load_config(self, filename):
        with open(filename, 'r') as stream:
            try:
                return(yaml.load(stream))
            except yaml.YAMLError as exc:
                print(exc)
                # TODO RD : Test method to validate configuration

    def _load_ssh_key(self):
        print("\
#============================================================================\n\
# You choose to use SSH method to clone your repos, in order to clone them\n\
# easier your SSH key will be loaded with keychain.\n\
# You will be asked to enter your SSH key password.\n\
#==============================================================================")
        ssh_files = os.listdir(self.home_dir + "/.ssh/")
        pub_rsa_files = ""
        for i in ssh_files:
            if '.pub' in i:
                file=self.home_dir + "/.ssh/" + i
                pub_rsa_files = pub_rsa_file + " " + file
        os.system("eval '$(keychain --eval --agents ssh " + pub_rsa_files + " )'")


    def _setup_git(self, repo):
        """
        TODO RD : Write doc
        """
        giturl = []
        sub_giturl = []
        for iRemote in repo["remote"]:
            git = {}
            if "ssh_url" in iRemote:
                url=urlparse("ssh://" + iRemote["ssh_url"])
                git['url'] = url.netloc + url.path
            else:
                url=urlparse(iRemote["https_url"])
                git['url'] = url.scheme + "://" + url.netloc + url.path
            git['domain'] = url.hostname
            git['user'] = url.username
            git['repo'] = url.path
            giturl.append(git)

        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                for iRemote in iSubdir["remote"]:
                    if "ssh_url" in iRemote:
                        url=urlparse("ssh://" + iRemote["ssh_url"])
                    else:
                        url=urlparse(iRemote["https_url"])
                    git = {}
                    git['url'] = url.netloc + url.path
                    git['domain'] = url.hostname
                    git['user'] = url.username
                    git['repo'] = url.path
                    iSubdir['giturl'] = git

        f = open(self.mr_dir + 'repo.d/' + repo['name'] + "." + repo["type"], 'w' )
        f.write("[" + repo["path"] + "/" + repo['name'] + "]\n")
        f.write("checkout    =\n")
        f.write("\t\techo '[0;32mClone [1;32m" + repo["name"]
                + " [0;32mfrom " + giturl[0]['domain'] + "[0m';\n")
        if "checkout_cmd" in repo:
            f.write("\t\t" + repo["checkout_cmd"] + ";\n")
        f.write("\t\tgit clone " + giturl[0]['url'] + " "
                + repo["name"] +";\n")
        f.write("\t\tcd " + repo["path"] + "/" + repo["name"] + ";\n")
        f.write("\t\tgit config user.email " + repo["usermail"]
                +";\n")
        f.write("\t\tgit config user.name " + repo["username"]
                +";\n")
        f.write("\t\tgit remote remove origin\n")
        for iGiturl in giturl:
            f.write("\t\techo '[0;32mAdding remote [1;32m" + iGiturl['domain']
                    + "[0m';\n")
            f.write("\t\tgit remote add "
                    + iGiturl['domain'] + " " + iGiturl['url'] + ";\n")
        f.write("\t\tgit push -u "
                + giturl[0]['domain'] + " master;\n")
        f.write("\t\techo '[0;32mFetch all remote branches from [1;32m"
                + iGiturl['domain'] + "[0m';\n")
        f.write("\t\tgit pull --all;\n")
        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                f.write("\t\techo '[0;32mClone [1;32m" + iSubdir["name"]
                        + " [0;32mfrom " + iSubdir["giturl"]['domain'] + "[0m';\n")
                f.write("\t\tmkdir -p " + iSubdir["path"] + ";\n")
                f.write("\t\tgit clone " + iSubdir["giturl"]["url"] + " " + iSubdir["path"]
                    + "/" + iSubdir["name"] + ";\n")
        f.write("pull        =\n")
        f.write("\t\techo '[0;32mPull [1;32m" + repo["name"]
                + " [0;32mfrom " + giturl[0]['domain'] + "[0m';\n")
        f.write("\t\tgit pull -q;\n")
        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                f.write("\t\techo '[0;32mPull [1;32m" + iSubdir["name"]
                        + " [0;32mfrom " + iSubdir['giturl']['domain'] + "[0m';\n")
                f.write("\t\tcd " + iSubdir["path"] + "/" + iSubdir["name"] + ";\n")
                f.write("\t\tgit pull -q;\n")
        f.write("update      =\n")
        f.write("\t\techo '[0;32mPull [1;32m" + repo["name"]
                + " [0;32mfrom " + giturl[0]['domain'] + "[0m';\n")
        f.write("\t\tgit pull -q;\n")
        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                f.write("\t\techo '[0;32mPull [1;32m" + iSubdir["name"]
                        + " [0;32mfrom " + iSubdir['giturl']['domain'] + "[0m';\n")
                f.write("\t\tcd " + iSubdir["path"] + "/" + iSubdir["name"] + ";\n")
                f.write("\t\tgit pull -q;\n")
        f.write("push        =\n")
        for iGiturl in giturl:
            f.write("\t\techo '[0;32mPush [1;32m" + repo["name"] + " [0;32mto "
                    + iGiturl['domain'] + "[0m';\n")
            f.write("\t\tgit push "
                    + iGiturl['domain'] + ";\n")
        f.write("remote      =\n")
        f.write("\t\tgit remote -v show;\n")
        f.write("delete      =\n")
        f.write("\t\techo '[0;31mWARNING ! Will delete [1;31m" + repo["name"]
                + ".[0m';\n")
        f.write("\t\techo 'Press [0;31mEnter[0m to continue or \
[0;32mCtrl-D[0m to abort';\n")
        f.write("\t\tread yn;\n")
        f.write("\t\techo '[0;31mDelete [1;31m" + repo["name"] + "[0;31m.[0m';\n")
        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                f.write("\t\techo '[0;31mDelete [1;31m" + iSubdir["name"]
                        + "[0;31m.[0m';\n")
                f.write("\t\trm -rf " + iSubdir["path"] + "/" + iSubdir["name"]
                        + ";\n")
        f.write("\t\trm -rf " + repo["path"] + "/" + repo["name"]
                + ";\n")


    def _setup_vcsh(self, repo):
        """
        TODO RD : Write doc
        """
        giturl = []
        sub_giturl = []
        for iRemote in repo["remote"]:
            git = {}
            if "ssh_url" in iRemote:
                url=urlparse("ssh://" + iRemote["ssh_url"])
                git['url'] = url.netloc + url.path
            else:
                url=urlparse(iRemote["https_url"])
                git['url'] = url.scheme + "://" + url.netloc + url.path
            git['domain'] = url.hostname
            git['user'] = url.username
            git['repo'] = url.path
            giturl.append(git)


        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                for iRemote in iSubdir["remote"]:
                    if "ssh_url" in iRemote:
                        url=urlparse("ssh://" + iRemote["ssh_url"])
                    else:
                        url=urlparse(iRemote["https_url"])
                    git = {}
                    git['url'] = url.netloc + url.path
                    git['domain'] = url.hostname
                    git['user'] = url.username
                    git['repo'] = url.path
                    iSubdir['giturl'] = git

        f = open(self.mr_dir + 'repo.d/' + repo['name'] + "." + repo["type"], 'w' )
        f.write("[" + self.vcsh_dir + "repo.d/" + repo['name'] + ".git]\n")
        f.write("checkout    =\n")
        f.write("\t\techo '[0;32mClone [1;32m" + repo["name"]
                + " [0;32mfrom " + giturl[0]['domain'] + "[0m';\n")
        if "checkout_cmd" in repo:
            f.write("\t\t" + repo["checkout_cmd"] + ";\n")
        f.write("\t\tvcsh clone " + giturl[0]['url'] + " "
                + repo["name"] +";\n")
        f.write("\t\tvcsh " + repo["name"] + " config user.email " + repo["usermail"]
                +";\n")
        f.write("\t\tvcsh " + repo["name"] + " config user.name " + repo["username"]
                +";\n")
        f.write("\t\tvcsh " + repo["name"] + " remote remove origin\n")
        for iGiturl in giturl:
            f.write("\t\techo '[0;32mAdding remote [1;32m" + iGiturl['domain']
                    + "[0m';\n")
            f.write("\t\tvcsh " + repo["name"] + " remote add "
                    + iGiturl['domain'] + " " + iGiturl['url'] + ";\n")
        f.write("\t\tvcsh " + repo["name"] + " push -u "
                + giturl[0]['domain'] + " master;\n")
        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                f.write("\t\techo '[0;32mClone [1;32m" + iSubdir["name"]
                        + " [0;32mfrom " + iSubdir["giturl"]['domain'] + "[0m';\n")
                f.write("\t\tmkdir -p " + iSubdir["path"] + ";\n")
                f.write("\t\tgit clone " + iSubdir["giturl"]["url"] + " " + iSubdir["path"]
                    + "/" + iSubdir["name"] + ";\n")
        f.write("\t\techo '[0;32mPull all remote branch for [1;32m"
                + repo["name"] + " [0;32mfrom " + giturl[0]['domain'] + "[0m';\n")
        f.write("\t\tvcsh " + repo["name"] + " pull --all;\n")
        f.write("pull        =\n")
        f.write("\t\techo '[0;32mPull [1;32m" + repo["name"]
                + " [0;32mfrom " + giturl[0]['domain'] + "[0m';\n")
        f.write("\t\tvcsh " + repo["name"] + " pull -q;\n")
        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                f.write("\t\techo '[0;32mPull [1;32m" + iSubdir["name"]
                        + " [0;32mfrom " + iSubdir['giturl']['domain'] + "[0m';\n")
                f.write("\t\tcd " + iSubdir["path"] + "/" + iSubdir["name"] + ";\n")
                f.write("\t\tgit pull -q;\n")
        f.write("update      =\n")
        f.write("\t\techo '[0;32mPull [1;32m" + repo["name"]
                + " [0;32mfrom " + giturl[0]['domain'] + "[0m';\n")
        f.write("\t\tvcsh " + repo["name"] + " pull -q;\n")
        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                f.write("\t\techo '[0;32mPull [1;32m" + iSubdir["name"]
                        + " [0;32mfrom " + iSubdir['giturl']['domain'] + "[0m';\n")
                f.write("\t\tcd " + iSubdir["path"] + "/" + iSubdir["name"] + ";\n")
                f.write("\t\tgit pull -q;\n")
        f.write("push        =\n")
        for iGiturl in giturl:
            f.write("\t\techo '[0;32mPush [1;32m" + repo["name"] + " [0;32mto "
                    + iGiturl['domain'] + "[0m';\n")
            f.write("\t\tvcsh " + repo["name"] + " push "
                    + iGiturl['domain'] + ";\n")
        f.write("remote      =\n")
        f.write("\t\tvcsh " + repo["name"]
                + " remote -v show;\n")
        f.write("delete      =\n")
        f.write("\t\techo '[0;31mWARNING ! Will delete [1;31m" + repo["name"]
                + ".[0m';\n")
        f.write("\t\techo 'Press [0;31mEnter[0m to continue or \
[0;32mCtrl-D[0m to abort';\n")
        f.write("\t\tread yn;\n")
        if "subdir" in repo:
            for iSubdir in repo["subdir"]:
                f.write("\t\techo '[0;31mDelete [1;31m" + iSubdir["name"]
                        + "[0;31m.[0m';\n")
                f.write("\t\trm -rf " + iSubdir["path"] + "/" + iSubdir["name"]
                        + ";\n")
        f.write("\t\tvcsh delete " + repo["name"] + ";\n")
        f.write("\t\techo '[0;31mDelete [1;31m" + repo["name"] + "[0;31m.[0m';\n")

    def _choose_repo(self, repo_type):
        count = 0
        choice_list = ""
        conf_file = self.mr_dir + "config.d/" + socket.gethostname() + "_" \
                + repo_type + ".cfg"
        cmd="whiptail --title 'Init Host myRepo' --checklist  'Select which " \
            + repo_type + "repo you want you want for this host :' "
        for iRepo in self.conf["repos"]:
            if iRepo["type"] == repo_type:
                active = False
                if os.path.exists(conf_file) and os.path.isfile(conf_file):
                    file = open(conf_file, "r")
                    for line in file:
                        if re.search(iRepo["name"] + "." + repo_type, line):
                            active = True
                    file.close()
                if active == True:
                    choice_list += "'" + iRepo["name"] + "' '" + iRepo["desc"] \
                    + "' 'ON' "
                else:
                    choice_list += "'" + iRepo["name"] + "' '" + iRepo["desc"] \
                    + "' 'OFF' "
                count += 1 ;
        cmd += str(count * 2 + 6) + " 80 " + str((count - 1) * 2 + 1) + " " + choice_list
        print(cmd)
        input()
        os.system("bash -c \"" + cmd + "\" 2> " + self.mr_dir + "results_menu.txt")
        if os.path.isfile(self.mr_dir + "results_menu.txt"):
            f = open(conf_file, "w")
            res = open(self.mr_dir + "results_menu.txt")
            for line in res:
                for iRepo in self.conf["repos"]:
                    if iRepo["name"] in line:
                        f.write("include = cat " + self.home_dir
                                + "/.config/mr/repo.d/" + iRepo["name"] + "."
                                + repo_type + "\n")
            f.close()
            os.system("rm -f " + self.mr_dir + "results_menu.txt")

    def run(self):
        for iRepo in self.conf['repos']:
            if iRepo["type"] == "vcsh":
                self._setup_vcsh(iRepo)
            else:
                self._setup_git(iRepo)
        self._choose_repo("vcsh")
        self._choose_repo("git")
        conf_file = self.mr_dir + "config.d/" + socket.gethostname() + ".cfg"
        conf_file_vcsh = self.mr_dir + "config.d/" + socket.gethostname() \
                         + "_vcsh.cfg"
        conf_file_git = self.mr_dir + "config.d/" + socket.gethostname() \
                        + "_git.cfg"
        file = open(conf_file, "w")
        if os.path.exists(conf_file_vcsh):
            file.write("include = cat " + conf_file_vcsh + "\n")
        if os.path.exists(conf_file_git):
            file.write("include = cat " + conf_file_git + "\n")


mr_file = os.path.expanduser('~') + "/.config/mr/repo.yaml"
mr = MyRepo(mr_file)
mr.run()

